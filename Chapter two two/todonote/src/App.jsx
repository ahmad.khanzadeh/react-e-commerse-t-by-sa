import { useState ,useReducer } from 'react';
import './App.css';
import AddNew from './components/AddNew';
import NoteList from './components/NoteList';
import Notestatus from './components/Notestatus';
import Noteheader from './components/Noteheader';
import { NotesProvider } from './context/NotesContext';


function notesReducer(state,action){
  switch(action.type){
    case "add": {
      return[...state,action.payLoad];
    }
    case "delete":{
      return state.filter((s) => s.id !==action.payLoad);
    }
    case "complete":{
      return state.map((note) => note.id=== action.payLoad ? {...note, completed:!note.completed}: note);
    }
    default : throw new Error("unknown Error" + action.type);
  }
}

function App() {
// since both AddNex and NoteList need the former entered data, we have to lift 
//        the state and setState which store data( at first we develop it in one of the childs
//            then we shift it here)

//  const [notes, setNotes]=useState([]);
const [notes, dispatch]=useReducer(notesReducer,[]);
// to sort data in Noteheader.jsx
const [sortBy, setSortBy] = useState("latest");

// to pass new note to former notes
const handleNotes=(someNewNoteInChild) =>{
  // setNotes((prevNotes) => [...prevNotes, someNewNoteInChild]);
  dispatch({type:"add", payLoad: someNewNoteInChild});
}
// if user click a button to delete the note
const handleDeleteNote=(id) =>{
  // select all notes but the one which is selected
    // const filteredNotes = notes.filter((n) => n.id !==id);
    // write them down again
    // setNotes(filteredNotes);

    //setNotes((prevNotes)=> prevNotes.filter((n) => n.id !== id));

    // move logic to the reducer function 
    dispatch({type:'delete', payLoad:id})
};

const handleCheckNote =(e) =>{
    
    const noteId = Number(e.target.id);
    // const newNotes = notes.map(n => n.id === noteId ? {...n,completed:!n.completed}: n)
    // setNotes(newNotes);
    
    //setNotes((prevNotes) => prevNotes.map(n => n.id=== noteId ? {...n, completed:!n.completed}: n))
    // reducer function 
    dispatch({type:"complete", payLoad:noteId});
  }

  return (
    <NotesProvider>
    <div className="container">
      {/* <div className="note-header">Note header</div> */}
      <Noteheader notes={notes} sortBy={sortBy} onSort={e => setSortBy(e.target.value)}/>
      <Notestatus notes={notes}/> 
      <div className="note-app">
        <AddNew onAddNote={handleNotes} />
        <div className="note-container">
          <NoteList notes={notes} sortBy={sortBy} onDelete={handleDeleteNote} onChecke={handleCheckNote}/>
        </div>
      </div>
    </div>
    </NotesProvider>

  )
}

export default App
