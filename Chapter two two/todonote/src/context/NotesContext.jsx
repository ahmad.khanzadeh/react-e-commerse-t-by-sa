import { useContext } from "react";
import { createContext , useReducer} from "react";


const NotesContext= createContext(null);
const NotesDispatchContext= createContext(null);


function notesReducer(state,action){
    switch(action.type){
      case "add": {
        return[...state,action.payLoad];
      }
      case "delete":{
        return state.filter((s) => s.id !==action.payLoad);
      }
      case "complete":{
        return state.map((note) => note.id=== action.payLoad ? {...note, completed:!note.completed}: note);
      }
      default : throw new Error("unknown Error" + action.type);
    }
  }

//provider
 export function NotesProvider({children}){
    const [notes, dispatch]=useReducer(notesReducer,[]);

    return(<NotesContext.Provider value={notes}>
            <NotesDispatchContext.Provider value={dispatch}>
                {children}
            </NotesDispatchContext.Provider>
    </NotesContext.Provider>);
}
//custome hook
export function useNotes(){
    return useContext(NotesContext);
}
//custom hook for dispatch 
export function useNotesDispatch(){
    return useContext(NotesDispatchContext)
}