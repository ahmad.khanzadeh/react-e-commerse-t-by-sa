import React from 'react'
import Message from './Message';

const Notestatus = ({notes}) => {
    const allNotes=notes.length;
    const completedNotes=notes.filter( n => n.completed===true).length
    const unCompletedNotes=allNotes-completedNotes;

    // if there were no notes, we just return an empty object about it
    if(!allNotes) return <Message icon="$" text="No notes has already been added.">This is where children comes true</Message> 
    return (
    <ul className='note-status'>
        <li> All <span>{allNotes}</span></li>
        <li>completed <span>{completedNotes}</span></li>
        <li>Open <span>{unCompletedNotes}</span></li>
    </ul>
  )
}

export default Notestatus