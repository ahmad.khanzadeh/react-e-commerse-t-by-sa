import React from 'react';


const Noteheader = ({notes,sortBy, onSort}) => {
    // when there is a checkbox, select box and .... always controll it by onChange
    
    

  return (
    <div className="note-header">
    <h2>Notes({notes.length})</h2>
        <select value={sortBy} onChange={onSort}>
            <option value="latest">Sort based on latest notes</option>
            <option value="earliest">Sort based on earliest notes</option>
            <option value="completed">Sort based on completed notes</option>
        </select>
    </div>
  )
}

export default Noteheader