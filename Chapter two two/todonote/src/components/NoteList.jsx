import React from 'react'

const NoteList = ({notes, onDelete, onChecke,sortBy}) => {
   //based on selected option on header, the sorting note will be changed - it is passed to this file from app.jsx
// since we don't want to mutate the original array, we have to make a shallow copy of 
// our array

    let sortednotes = notes;
    
if(sortBy==="earliest")
 sortednotes=[...notes].sort(
(a,b) => new Date(a.createdAt)- new Date(b.createdAt)
); // a-b => a>b ? 1 : -1

if(sortBy==="latest")
 sortednotes=[...notes].sort(
(a,b) => new Date(b.createdAt)- new Date(a.createdAt)
); //b-a => a>b ? -1 : 1

if(sortBy==="completed")
sortednotes=[...notes].sort(
  (a,b)=> Number(b.completed) - Number(a.completed)
) 

  return (
    <div className="note-list">
        {sortednotes.map((note) =>(
            <NoteItem key={note.id} note={note} onDelete={onDelete} onChecke={onChecke}/>
        ))}
    </div>
  )
}

export default NoteList


function NoteItem({ note , onDelete , onChecke}){
    // for dating function
    const options={
        year:"numeric",
        month:"long",
        day:"numeric",
    };

    return(
            <div className={`note-item ${note.completed ? "completed" : " " }`}>
                <div className="note-item_header">
                    <div>
                        <p className="title">{note.title}</p>
                        <p className="desc">{note.description}</p>
                    </div>
                    <div className="actions">
                        <button onClick={() => onDelete(note.id)}> &#10060; </button>
                        {/* since it is an input, data will be passed by event (e) ---no need to pase note.id */}
                        <input onChange={ onChecke } type="checkbox" name={note.id} id={note.id} value={note.id} />
                    </div>
                </div>
                <div className="note-item_footer">
                    {new Date(note.createdAt).toLocaleDateString('de-DE', options)}
                </div>
            </div>
    );
}