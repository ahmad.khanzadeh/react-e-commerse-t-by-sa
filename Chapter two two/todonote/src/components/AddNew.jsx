import React from 'react'
import { useState } from 'react';

const AddNew = ({onAddNote}) => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("")
    

    const submitHandler=(e) =>{
        // if user did not add anything, dont render the following boxes
        if(!title || !description) return null;

        e.preventDefault();
        // not to refresh the page
        // submit all data to a json format
        const newNote ={
            title,
            description,
            id: Date.now(),
            completed: false,
            createdAt: new Date().toISOString(),
        }
        
        // ok, we pass the data, now it is time to clear the input fields for next task
        setTitle("");
        setDescription("");
        // add this new note to older notes
        // setNotes((prevNotes) => [...prevNotes, newNote]);
        onAddNote(newNote);
    }

    const handleChange =(e) =>{
        // console.log(e.target.value);
        setTitle(e.target.value);
    }

    const handleDescription =(e) =>{
        // console.log(e.target.value);
        setDescription(e.target.value);
    }
  return (
    <div className="add-new-note">
        <h1> Add new Note</h1>
        <form className="note-form" onSubmit={submitHandler}>
            <input value={title} onChange={handleChange} type="text" className='text-field' placeholder='The name of the job'/>
            <input value={description} onChange={handleDescription} type="text" className='text-field' placeholder='details'/>
            <button type="submit" className='btn btn--primary'>Add New note</button>
        </form>
    </div>
  )
}

export default AddNew