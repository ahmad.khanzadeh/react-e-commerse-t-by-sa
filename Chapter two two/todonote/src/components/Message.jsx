
const Message = ({text,icon,children}) => {
  return (
    <div>{icon}-{text}-{children}</div>
  )
}

export default Message