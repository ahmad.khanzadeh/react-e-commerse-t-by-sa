import './App.css'
import Accordion from './assets/component/Accordion'

function App() {

  return (
    <div> 
      <Accordion />
    </div>
  )
}

export default App
