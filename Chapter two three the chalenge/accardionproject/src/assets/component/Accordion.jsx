import { useState } from 'react';
import { ChevronDownIcon, ChevronUpDownIcon } from '@heroicons/react/24/solid'

const data=[
    {
        id:1,
        title: "Accordion One",
        text:"some related text for accordion for number 1"
    },
    {
        id:2,
        title: "Accordion two",
        text:"some related text for accordion number 2 that I currently add it to the page"
    },
    {
        id:3,
        title: "Accordion Three",
        text:"some related text for accordion number 3 which is not from server"
    },
   
]

const Accordion = () => {
    // other accordions should be closed by using this accordions
    const [open, setOpen] = useState(null) // id of displayed
  return (
    <div className="accordion">
         {data.map((item)=>(<AccordionItem 
          setOpen={setOpen}
          item={item}
          key={item.id}
          open={open}
            >{item.text}</AccordionItem>))}
     </div>
  )
}

export default Accordion

function AccordionItem({item, open, setOpen, children}) {
    /* 
         1)in data az data haye digeh sakhteh mishe? Na
         2) in data dar tool omr barname taghyir mikoneh? bale
         3) taghyiir in data baes rerender shodan mishe? Bale!
    */
//    const [isOpen,setIsOpen]=useState(false)

// in case only one option should be visible for the users: 
const isOpen = item.id===open ? true : false;

    return(<div className={`accordion-item ${isOpen ? "accordion__expanded": " "}`}>
                 <div className='accordion-item__header' onClick={()=> setOpen(item.id)}>
                 <div>{item.title}</div>
                 <ChevronDownIcon 
                 className="accordion-item__chevron"
                 style={{
                    width: "1.2rem",
                     transition: "all 0.2s ease-out",
                     rotate: isOpen ? "180deg" : "0deg",
                     }} />
                 </div>
                {isOpen && <div className='accordion-item__content'>{children}</div>}
         </div>
    );
}