import React from 'react';
import { Link } from 'react-router-dom';

const Posts = () => {
  return (
    <div>
      <h1>Posts Page</h1>
      <nav>
        <ul>
          <li>
             <Link to="/app/posts/1">Post 1</Link>
          </li>
          <li>
             <Link to="/app/posts/2">Post 2</Link>
          </li>
          <li>
              <Link to="/app/posts/3">Post 3</Link>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default Posts