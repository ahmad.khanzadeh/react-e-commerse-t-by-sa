import React from 'react'
import { useNavigate, Navigate } from 'react-router-dom';
import {useState} from "react";

const Login = () => {
const navigate =useNavigate();
const [isAuth,setIsAuth]=useState(true);

    const handleSubmit=(e) =>{
        e.preventDefault();
        //push user to dashboard, replacement: true will make the former path unaccessible,(like login component)
        navigate("/app/dashboard",{replace:false, state:"this is the information to send to reciver-here the dashboard"})
    }   
  return (
    <div>
        <h1>Login</h1>
        {isAuth && <Navigate to="/app/dashboard" replace={true}></Navigate>}
        <form onSubmit={handleSubmit}>
            <button>Login</button>
        </form>
        <button onClick={()=> setIsAuth(!isAuth)}>Toggle the login condition</button>
    </div>
  )
}

export default Login