import React from 'react'
import { NavLink, Outlet, useLocation} from 'react-router-dom'

const Dashboard = () => {
  // to receive data which we get it from our component
  const location =useLocation();
  console.log(location);

  return (
    <div id="dashboard">
      <div id="sidebar">
        <NavLink to="Profile">Profile</NavLink>
        <NavLink to="Payment">Payment</NavLink>
      </div>
      <Outlet />
    </div>
  )
}

export default Dashboard