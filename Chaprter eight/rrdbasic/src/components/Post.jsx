import React from 'react'
import { useParams , Link, useSearchParams} from 'react-router-dom'

const allPosts=[
    {
        id:1,
        title:"title one",
        body: "Body one",
    },
    {
        id:2,
        title:"title two",
        body: "Body two",
    },
    {
        id:3,
        title:"title three",
        body: "Body three",
    },
]
const Post = () => {
    const params=useParams();
    // console.log(params);
    // disclose the id, since we pass the (<Route to="/posts/:id" />)
    const [searchParams,setSearchParams]=useSearchParams();
    console.log(searchParams.get('type'));
    //it has lots of methods...search for them.
    //how to filter fetch data and display the one that matches the id
    const post= allPosts.find((p)=> p.id===Number(params.id));
  return (
    <div>Single Post comes here : {params.id}
        <p>{post.title}</p>
        <p>{post.body}</p>
        <Link to="/app/posts">Go to Posts</Link>
    </div>
  )
}

export default Post