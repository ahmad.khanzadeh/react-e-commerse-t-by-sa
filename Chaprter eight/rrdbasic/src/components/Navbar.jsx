import React from 'react'
import {Link, NavLink } from 'react-router-dom'

const Navbar = () => {
  return (
    <nav>
        <ul>
          <li>
                {/* <a href="/"> Home </a>
                <Link to="/">Home</Link> */}
                <NavLink end to=""> Home </NavLink>
           </li>
          <li>
                 {/* <a href="/posts">Posts</a> */}
                 <NavLink to="posts">Posts</NavLink>
          </li>
          <li>
                <NavLink to="dashboard">dashboard</NavLink>
          </li>
          <li>
                <NavLink to="login">Login</NavLink>
          </li>
        </ul>
      </nav>
  )
}

export default Navbar