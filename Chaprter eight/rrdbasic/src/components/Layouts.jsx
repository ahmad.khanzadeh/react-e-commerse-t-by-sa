import React from 'react'
import Navbar from './Navbar'
import { Outlet } from 'react-router-dom'

const Layouts = () => {
  return (
    <div>
        <Navbar />
        {/* the dynamic section will come here  */}
        <Outlet />
        <footer
        style={{marginTop:"5rem"}}
        >
         this is the footer section. imagin it is a separeted component
         </footer>
    </div>
  )
}

export default Layouts