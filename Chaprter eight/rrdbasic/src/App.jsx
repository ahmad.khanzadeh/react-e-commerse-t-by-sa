import './App.css'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import  Home  from './pages/Home';
import  Dashboard  from './pages/Dashboard';
import  Posts  from './pages/Posts';
import Profile from './components/Profile';
import Payment from './components/Payment';
import Layouts from './components/Layouts';
import Post from './components/Post';
import Login from './pages/Login';

function App() {
  return (
   <BrowserRouter>
      <div>
        <Routes>
        {/* nested routes for navbar and footer */}
          <Route path="/app" element={<Layouts />}>
               <Route index element={<Home />}/>
               <Route path="login" element={<Login />} />
               <Route path="posts/:id" element={<Post/>} />
               <Route path="posts" element={<Posts/>} />
               <Route path="dashboard" element={<Dashboard />} >
                  <Route index element={<Navigate to="profile" />} />
                  <Route path="profile" element={<Profile />} />
                  <Route path="payment" element={<Payment />} />
               </Route>
          </Route>
        </Routes>
      </div>

   </BrowserRouter>
  );
}

export default App
