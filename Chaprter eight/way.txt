//====================================
//92
//====================================

    Kholaseh: mikhaym Navbar besazim ke karbar rosh click kard biyad 
    1) react router dom ro nasb kon to terminal :  npm i react-router-dom@latest
    2) hame component ha bayad dakhel yeki az ina gharar begiran: ----------------<BrowserRouter> </BrowserRouter>
                                                                                    import {BrowserRouter} from 'react-router-dom'
    hala in ro mikahy to App.jsx bezaresh( ke hame berizan tosh) ja to main.jsx. faghat har ja bood yadet bashe impoet koni
    3) hala on navbar ro adi besaz: <nav> <ul> <li> <a href="/post">Post</a> </li>  <li>....</li></ul></nav>
    4)hala boro on eleman hayi ke gharare be on <nav> neshon dadde beshe ro to ye poshe be esm pages besaz.( ba hamon dastor rafce adi besazeshon)

    4)dakhel hamon <BrowswerRouter> bayad ye eleman <Routes> besazim ke to on tamam <Route> ha ro gharar bedim . inja in shekli shod

            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/post" element={<Post />} />
                    <Route path="/dashbord" element={<Dashbord />}
                </Routes>
            </BrowserRouter>


    alan yani to adressbar age taraf zad /post bayad eleman <Post /> jaye hame ye in eleman ha neshon dade beshe. 
    3 ta nokte mimoneh faghat: a) import {Route, Routes}  from 'react-router-dom'
                               b) import Post from './pages/Post' age ino to {} bezari dorost kar nemikoneh

//==============================
//93
//==============================


    age to har bakhshi as tag <a href=""> </a> estefade konim application baramon reload mishe va in be dardemon nemikhoreh
    in rah hal ha ro har ja bodim react-router-dom baramon gozashteh

                <a href="/"> Home </a>
                <Link to="/">Home</Link>
                <NavLink to="/"> Home </NavLink>

    har se ta be yek mani and . faghat a) 2 ta compoenent ro ke to edame avordam bayad import koni
                                            import {Link, NavLink } from 'react-router-dom'
                                        b)on NavLink ye component khaseh ke age karbar ro har kodom click 
                                          kard, ye class be esm .active behesh ezafe mikoneh.(style bedim
                                          ya search koni ke how to style NavLink mesal hasho barat miyare)

    be in model address dehi ham migan absolute ( in shekli : to="/dashbord")
    model relative ham hast (tp="dashbord") =====> in do ta ro havaset bashe ghati nakonim-in mige masiri ke alan tosh hastim chiye? boro to hamin masir
                                                    donbal ye eleman begard.


//======================================
//94
//======================================

On app.jsx ziyadi shologhe. mikahn compoentn haye navbar, footer ro azash bebaram biron:
    1) ye file dorost kon be esm Layout.jsx va hame ye component ha ro beriz tosh
      (ba hamon rafce)
      in ha bayad to on bashen: 


      import { Outlet } from 'react-router-dom'

        const Layouts = () => {
            return (
                 <div>
                     <Navbar />
                     <Outlet />
                     <footer>this is the footer section. imagin it is a separeted component </footer>
                 </div>
                 )
        }

        export default Layouts

    oon Outlet kar hamon children ro mikoneh, yani har jadi seda zadanemon , har chizi ke be esm value
    behemon dadan ro beriz in to

    hala to on file App.jsx mikhaym in ha ro vared konim : 
        <Routes>
          <Route element={<Layouts />}>
               <Route  index element={<Home />}/>
               <Route path="posts" element={<Posts/>} />
               <Route path="dashboard" element={<Dashboard />} />
          </Route>
        </Routes>

    har 3 ta route ghabli ro to in <Route> jadid gozashtim va ye elemnet={<Layout />}
    dadim ke yani har chizi ke in payin hast ro to oon element gharar bedeh.

oon <Route index value={}> jani age adressi vared nashode bood bedon in elemnt asliye
(pishfarz hamon ro to Route asli kenar header va footer neshon mideh)


in ke migoftim behtareh ke relative bashe daghigan inja shoro mishe. age in <Route path="/App" >
dashete bashe( baraye khodesh ye path khas dashteh) oon vaght bayad Route tak tak on ha ro berim avaz konim 
pas 
az aval biya path="post" va ... bezar ta relative beshe va age masir valed avaz shod, automatic ina ham avaz beshan.




//===========================================
//95
//===========================================


    ta inja omadim navbar ro sakhtim ke ro har kodom az ona click konim be ye adressi mirim. hala to on adress farzan ham mikam ye box bezaram ke 
    karbar ro har kodom click kard etelaat oon bakhsh barash biyad ( yani to yeki as <Route /> ha mi khaym chand ta <Route /> bezarim)

    eghadamat:
            1) to folder components do ta component besaz ba rafce (inja Payment va Profile)
            2) to component Dashboard.jsx 2 ta <NavLink > besaz ke behesh link bedan (<Outlet /> yadet nareh)
                 in shekli:   <div id="dashboard">
                                <div id="sidebar">
                                     <NavLink to="Profile">Profile</NavLink>
                                     <NavLink to="Payment">Payment</NavLink>
                                </div>
                                <Outlet />
                              </div>

            3)to App.jsx on < Route /> ke be component Dashboard miresid ro in shekli avaz kon: 

                ghadim:     <Route path="dashboard" element={<Dashboard />}  />

                alan:        <Route path="dashboard" element={<Dashboard />} >
                                <Route path="profile" element={<Profile />} />
                                <Route path="payment" element={<Payment />} />
                            </Route>


//=================================================
//96
//=================================================
dynamic Routes
    to ye site vared bakhs haye mokhtalef mishi ( masalan product hashon) - ghaleb sabete va baste be url daran data az back migiran neshon midan
    hamon ro mikhaym besazim

    1) to hamon App.jsx( har jayi ke <Route > neshon dadan in safeh bood) ye <Route > jadid besaz
        <Route path="posts/:id" element={<Post/>} />
    
    2)ye component to file components besaz be esm Post.jsx baad rafce
        import React from 'react'
        import { useParams} from 'react-router-dom'

const Post = () => {
    const params=useParams();
    console.log(params);
    // disclose the id, since we pass the (<Route to="/posts/:id" />)

  return (
    <div>Single Post comes here : {params.id}</div>
  )
}

export default Post

        params ye hoock hast ke etelaat on route (khod navar adress) ro baramon miyare
        hala baste be in etelaat mitonim az backend data begirim. 

        mesal daryaft data to file Post.jsx khande shavad

    3)to file Posts.jsx ham ye tedad gozine bezar karbar click kone biyad to safe ina

          <li>
             <Link to="/app/posts/1">Post 1</Link>
          </li>



//==========================================
//97
//==========================================
store data in url
    1)params => /postId => /post/1, .....
    2)searchParams-(query-string) => example.com?type=front

    film ro baad ha dobare bebin har chand dorost tozih nadad



//=============================================
//98
//=============================================
    baad az ye seri event momkene bekhaym karbar ro be ye jaye dige befrestim. 
    in mishe programmatically navigation => same as Link
                        1. usenavigage() => imperative
                        2. Navigate compoenent => imperative
        
                    Link => declearative
    mikhaym vaghti karbar login kard automatic bere to ye safhe dige
    

    ye <Route /> jadid barash dorost mikonim to App.jsx + ye component jadid to pages be esm Dashboard bad rafce
    tosh ue <form unsubmit={handleSubmit}> </form> gozashtam. ama nokteh
    mikham karbar ro dokme to in form click kard be ye adress bereh. baad baraye onja ham ye seri etalaat ersal beshe( masalan etelaat in karbar inja)
    as ye hook jadid estefadeh mishe  be esm useNvigate() address migireh + ye araye:


            import { useNavigate } from 'react-router-dom';

            const Login = () => {
                    const navigate =useNavigate()

                    const handleSubmit=(e) =>{
                                 e.preventDefault();
                            //push user to dashboard, replacement: true will make the former path unaccessible,(like login component).state: what to send
                                 navigate("/app/dashboard",{replace:false, state:"this is the information to send to reciver-here the dashboard"})
                     }   
             return (
                     <div>
                          <h1>Login</h1>
                          <form onSubmit={handleSubmit}>
                                 <button>Login</button>
                          </form>
                     </div>
                     )
            }

        export default Login



//========================================================
//99
//========================================================

    khob age karbar login karde bood dige dashboard ro neshon nadeh: 
    useState barash sakhtim va ye shart kozashtim ke true beshe bere ye address dige ro neshon bedeh
    
    to component login in ezafe shod ke age vorod shode bood ye chiz dige neshon bedeh

    <div>
        <h1>Login</h1>
        {isAuth && <Navigate to="/app/dashboard" replace={true}></Navigate>}
        <form onSubmit={handleSubmit}>
            <button>Login</button>
        </form>
        <button onClick={()=> setIsAuth(!isAuth)}>Toggle the login condition</button>
    </div>


    to App.jsx ham ye <Route index element={<Navigate to="Profile" />} ke bere onja