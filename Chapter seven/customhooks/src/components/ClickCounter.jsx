import { useState, useRef } from "react";

export default function ClickCounter(){
    const [count, setCount]=useState(0);
    const ref=useRef(0);

    const handleClick=()=>{
        // setCount((c)=> c+1);
        ref.current= ref.current +1;
        console.log(ref.current);
    }
    console.log('rerendering...')
    return(<div>
            {/* <p>Count: {count} </p> */}
            <button onClick={handleClick}>Click me!</button>
    </div>
    );
}