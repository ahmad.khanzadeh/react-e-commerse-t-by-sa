import { useEffect ,useReducer,useState} from "react";

const initialState={ loading: true, data:null, error: false}


function fetchReducerFunction(state,action){
    switch(action.type){
        case "PENDING":{
            return {loading:true, data:null, error:false};
        }
        case "SUCCESS" :{
            return {loading:false, data:action.payload, error: false};
        }
        case "REJECT":{
            return {loading:false, data: null, error:true};
        }
        default:
            return state;
    }
}
export default function Post(){
    // const [loading, setLoading] = useState(true);
    // const[data, setData]=useState(null);
    // const [error, setError]=useState(false);
    const [state,dispatch]=useReducer(fetchReducerFunction,initialState)

    //remember to extract loading, data and error from state (above section)
        const{loading, data, error}=state;
    useEffect(()=>{
        // setLoading(true);
        // setData(null);
        // setError(false);
        dispatch({type:"PENDING"});
        fetch("https://jsonplaceholder.typicode.com/posts/1")
        .then((res)=>res.json())
        .then((res)=>{
            // setLoading(false);
            // setData(res);
            // setError(false);
            dispatch({type: "SUCCESS", payload: res});
        })
        .catch((err)=>{
            // setLoading(false);
            // setData(nall);
            // setError(true);
            dispatch({type:"REJECT", payload:err?.response?.message})
        });

    },[]);

    return(<div>
        {loading ? (<p>Loading ...</p>) : (<div><h1>{data.title}</h1><p>{data.body}</p></div>)}
        {error && <p>"An error occured"</p>}
    </div>);
}