import { useEffect, useRef , forwardRef } from "react";

export default function Focus(){
    const refValue=useRef();
  
   useEffect(()=>{
    // const elem=document.querySelector('.input-box');
    // elem.focus();
    
   },[])

   const handleFocus=()=>{
    refValue.current.value='Samyar khanzadeh'
    refValue.current.focus();
   }

    return(
        <div>
            <INput ref={refValue}  label=" The search console"/>
            {/* <input  className='input-box' type='text' ref={refValue} /> */}
            <button onClick={()=>handleFocus()}>Click here to handle Focus</button>
        </div>
    );
}

const INput=forwardRef(function Input(props,ref){
    return(<label>{props.label} <input className='input-box' type='text' ref={ref}/> </label>);
});