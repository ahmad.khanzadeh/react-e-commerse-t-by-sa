import { useState, useRef, forwardRef } from "react"

export default function Video(){
    const [isPlaying, setIsPlaying]=useState(false)
    const ref= useRef(null)

    // by clicking on the button => changing the UI => state
    //need access to an element => useRef()

    const handleClick=()=>{
        setIsPlaying(is => !is);
       
        if(isPlaying){
            ref.current.pause()
        }else{
            ref.current.play()
        }
    }
    
    return <>
        <button onClick={()=>handleClick() }>
           {isPlaying ? "pause": "play"}
        </button>
         
        {/* <video
            width="250"
            ref={ref}
            > 
            <source 
                src="https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.mp4"
                type="video/mp4"
            />
            </video> */}
            
            <VideoPlayer 
                ref={ref}
                width="250"
                type="video/mp4"
                src="https://interactive-examples.mdn.mozilla.net/media/cc0-videos/flower.mp4"
                />
    </>
}


const VideoPlayer=forwardRef((props,ref)=>{
    return(<video
        width={props.width}
        ref={ref}
        > 
        <source 
            src={props.src}
            type={props.type}
        />
        </video>
    );
})