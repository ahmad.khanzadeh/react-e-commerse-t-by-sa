import React, { useReducer } from 'react'
import { useState } from 'react'

const INITIALSTATE=0;
function reducerfunctionCountReducer(state,action){
    // if(action.type==='inc') return state+action.payLoad;
    // if(action.type==='dec') return state-action.payLoad;
    // if(action.type==='reset') return INITIALSTATE;
    switch(action.type){
        case "inc" :{
            return state+action.payLoad
        }
        case "dec" :{
            return state-action.payLoad
        }
        case "reset" :{
            return INITIALSTATE
        }
        default:{
            return state;
        }
    }

}
const Counter = () => {
    // const [count, setCount]= useState(INITIALSTATE);

    // const handleIncrement=()=>{
    //     setCount((c) => c+1)
    // }
    // const handleReset=()=>{
    //     setCount(INITIALSTATE)
    // }
    // const handleDecrement=() =>{
    //     setCount((c)=> c-1)
    // }

    const [count, dispatch]=useReducer(reducerfunctionCountReducer, INITIALSTATE)
    // steps: 1) call useReducer
    //        2)move logic to reducer function 
    //        3)dispatch action inside eventHandlers ==> what action happened?? ( dispatch= what action to triger)
    //        4)write logic inside reducer function based on each action
    //        5)return new state inside action as new state

    const handleIncrement=()=>{
        dispatch({type: "inc", payLoad:1});
    }
    const handleReset=()=>{
        dispatch({type: "reset",payLoad:0});
    }
    const handleDecrement=()=>{
        dispatch({type: "dec",payLoad:1});
    }
  return (
    <div>
        <div>Count : {count}</div>
        <button onClick={()=> handleIncrement()}>+</button>
        <button onClick={()=> handleReset()}>Reset</button>
        <button onClick={()=> handleDecrement()}>-</button>
    </div>
  )
}

export default Counter