import {useState, useRef} from "react"

export default function Stopwatch(){
    const [startTime, setStartTime]= useState(null);
    const [now, setNow]= useState(null);
    const intervalRef= useRef(null);
    // stop function is in another function, so we cant use useState( )
    // refrence ro bain do ta tabe enteghal midim

    const handleStart=() =>{
        setStartTime(Date.now());
        setNow(Date.now());

        // first clear formet interval for new interval counting
        clearInterval(intervalRef.current);
        
       intervalRef.current= setInterval(()=>{
            setNow(Date.now())
        },100)
    }

    const handleStop=()=>{
        clearInterval(intervalRef.current)
    }

    let secondPassed=0;
    if(startTime != null || now != null){
        secondPassed= (now - startTime)/1000
    }

    return(
        <>
            <h1>Time Passed : {secondPassed.toFixed(3)}</h1>
            <button onClick={handleStart}>Start</button>
            <button onClick={handleStop}>Stop</button>
        </>
    );
}