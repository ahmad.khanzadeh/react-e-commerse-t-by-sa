import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Focus from './components/Focus'
import  Video  from './components/Video'
import ClickCounter from './components/ClickCounter'
import Stopwatch from './components/StopWatch'
import Counter from './components/Counter'
import Post from './components/Post'

function App() {
  const [count, setCount] = useState(0)

  return (
   <div className='App'>
    {/* <Focus />  */}
    {/* <Video />  */}
    {/* <ClickCounter /> */}
    {/* <Stopwatch /> */}
    {/* <Counter /> */}
    <Post />
   </div>
  )
}

export default App
