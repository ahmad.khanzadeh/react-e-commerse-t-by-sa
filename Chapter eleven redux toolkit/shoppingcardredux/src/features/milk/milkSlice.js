import { createSlice } from "@reduxjs/toolkit";

const initialState={
    numOfMilks:10,
};

const milkSlice=createSlice({
    name:"milk",
    initialState:initialState,
    reducers:{
        buyMilk:(state) =>{
            // for milk we don't have any payload so just reduce the amount by one
            state.numOfMilks=state.numOfMilks-1
        },
    },
});

export const { buyMilk }= milkSlice.actions;

export default milkSlice.reducer;

