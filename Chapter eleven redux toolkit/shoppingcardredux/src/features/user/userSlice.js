import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getAsyncUsers = createAsyncThunk("user/getAsyncUsers", async(_,{rejectWithValue})=>{
    try{
        const response= await axios.get("https://jsonplaceholder.typicode.com/users");
        // return (builder)=> response.data;
        return response.data;
    } catch(error){
        return rejectWithValue(error.message);
    }
});

const initialState={
    loading:false,
    data: [],
    error:"",
};

const userSlice=createSlice({
    name:"user",
    initialState:initialState,
    extraReducers: builder => {
        builder.addCase(getAsyncUsers.pending,(state,action) =>{
            state.loading=true;
            state.data=[];
            state.error="";
        }, )
        // [getAsyncUsers.pending]: (state,action) =>{
        //     state.loading=true;
        //     state.data=[];
        //     state.error="";
        // },
        builder.addCase(getAsyncUsers.fulfilled,(state,action) =>{
            state.loading=false;
            state.data=action.paylod;
            state.error="";
        },)

        // [getAsyncUsers.fulfilled]: (state,action) =>{
        //     state.loading=false;
        //     state.data=action.paylod;
        //     state.error="";
        // },
        builder.addCase(getAsyncUsers.rejected,(state,action) =>{
            state.loading=false;
            state.data=[];
            state.error=action.paylod;
        },)

        // [getAsyncUsers.rejected]: (state,action) =>{
        //     state.loading=false;
        //     state.data=[];
        //     state.error=action.paylod;
        // },
    }
})


export default userSlice.reducer;

// //https://jsonplaceholder.typicode.com/users

