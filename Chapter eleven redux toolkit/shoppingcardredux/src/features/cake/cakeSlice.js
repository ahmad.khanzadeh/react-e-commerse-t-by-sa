import { createSlice } from "@reduxjs/toolkit";

const initialState={
    numOfCakes:10,
};

// export default function cakeReducer(state=initialState, action){
//     switch(action.type) {
//         case BUY_CAKE: {
//             return {
//                 ...state,
//                 numOfCakes:state.numOfCakes -action.payload,
//             };
//         }
//         default: return state;
//     }
// }

const cakeSlice=createSlice({
    name: "cake",
    initialState: {numOfCakes: 10},
    reducers: {
        buyCake:(state,action)=>{
            // we can immutate state directly
            state.numOfCakes=state.numOfCakes - action.payload;
        },
    },
});

// export action creators
export const {buyCake} = cakeSlice.actions;

export default cakeSlice.reducer;