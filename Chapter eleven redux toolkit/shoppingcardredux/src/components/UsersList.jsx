import React from 'react';
import { useEffect } from 'react';
import {useDispatch , useSelector} from 'react-redux';
import { getAsyncUsers } from '../features/user/userSlice';

const UserList = () => {
    const dispatch= useDispatch();
   

    useEffect(()=>{
        dispatch(getAsyncUsers());
    }, [dispatch]);

    // extract states from <rootReducer.js />
    const state=useSelector((state) => state.users);

  return (
    <div>userList
    {
      state.loading ? 
      (<p>Loading</p>) 
      : state.error ?
       (<p>{state.error}</p>)
       :
       (<div>
        {state.data && state.data.map(user => <li key={user.id}>{user.name}</li> )}
       </div>)
    }
    </div>
  )
}

export default UserList