import React , {useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { buyCake } from '../features/cake/cakeSlice';


const CakeContainer = () => {
    const [value, setValue]=useState(0);
    //1. the following hoock will accept a function as its parameter: selector function
    //2. this hook will return whatever returns by selector function
    const state=useSelector((state) => state.cake);
    
    const dispatch=useDispatch();
  return (
    <div>
        <div>
            <h2> Cake Number : {state.numOfCakes}</h2>
            <input type="text" value={value} onChange={(e)=> setValue(e.target.value)} />
            {/* <button onClick={()=> dispatch({type: "BUY_CAKE", payload:value})}>Buy Cake</button> */}
            <button onClick={()=> dispatch(buyCake(value))}>Buy Cake</button>
        </div>
    </div>

  )
}

export default CakeContainer