import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import axios from "axios";
// http://localhost:5000/todos

// make a base axios address
const api=axios.create({
    baseURL:"http://localhost:5000"
});

// getTodos
 export const getAsyncTodos=createAsyncThunk("todos/getAsyncTodos", async (payload,{rejectWithValue})=>{
    try{
       const response = await api.get("/todos");
       return response.data;
    } catch(error){
        return rejectWithValue(error.message);
    }
})

// addTodos
export const addAsyncTodos=createAsyncThunk("todos/addAsyncTodos", async (payload,{rejectWithValue})=>{
    try{
        const response = await api.post("/todos",{
            title:payload.title,
            id: Date.now(),
            completed:false,
        });
        return response.data;
    } catch(error){
        return rejectWithValue(error.message)
    }
})

//delete
export const deleteAsyncTodos = createAsyncThunk("todos/deleteAsyncTodos", async (payload,{rejectWithValue})=>{
    try{
      await api.delete(`/todos/${payload.id}`);
        return {id: payload.id};
        // later in extrareducer, the post with this id will be removed
    } catch (error){
        return rejectWithValue(error.message)
    }
})

//toggle
export const toggleAsyncTodos=createAsyncThunk("todos/toggleAsyncTodos", async (payload,{rejectWithValue}) => {
    try{
        const response=await api.patch(`/todos/${payload.id}`,{
            completed:payload.completed,
        });
        return response.data;
    } catch(error){
        return rejectWithValue(error.message)
    }
})


const todoSlice = createSlice({
    name:"todos",
    initialState: {
        todos:[],
        loading:false,
        error:"",
    },
    reducers:{
        addTodo:(state,action) =>{
            const newTodo={
                id: Date.now(),
                title:action.payload.title,
                completed:false,
            };
            state.todos.push(newTodo);
        },
        toggleTodo:(state,action) =>{
            const selectedTodo=state.todos.find((todo)=>todo.id===Number(action.payload.id))
            selectedTodo.completed=!selectedTodo.completed;
        },
        deleteTodo:(state,action) =>{
            state.todos = state.todos.filter((todo)=> todo.id !== Number(action.payload.id));
        }
    },
    extraReducers:{
        [getAsyncTodos.pending]: (state,action) => {
            state.loading=true;
            state.todos=[];
            state.error="";
        },
        [getAsyncTodos.fulfilled]: (state,action) => {
            state.loading=false;
            state.todos=action.payload;
            state.error=[];
        },
        [getAsyncTodos.rejected]: (state,action) => {
            state.loading=false;
            state.todos=[];
            state.error=action.payload;
        },
        [addAsyncTodos.pending]:(state,action)=>{
            state.loading=true;
        },
        [addAsyncTodos.fulfilled]:(state,action)=>{
            state.loading=false;
            state.todos.push(action.payload);
        },
        [addAsyncTodos.rejected]:(state,action)=>{
            state.loading=false;
            state.error=action.payload;
        },
        [deleteAsyncTodos.fulfilled]:(state,action)=>{
            state.loading=false;
            state.todos=state.todos.filter(todo => todo.id !==Number(action.payload.id));
        },
        [toggleAsyncTodos.fulfilled]:(state,action)=>{
            const selectedTodo=state.todos.find(todo => todo.id ===Number(action.payload.id));
            selectedTodo.completed=action.payload.completed;
        },
    }
})

export const {addTodo,deleteTodo,toggleTodo} = todoSlice.actions;

export default todoSlice.reducer;
