import React from 'react'
import TodoItem from './TodoItem';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getAsyncTodos } from '../../features/todo/todoSlice';


const TodoList = () => {
  // const todos=useSelector((state) => state.todos);
  const {todos , loading, error}=useSelector((state) => state.todos);
  const dispatch=useDispatch();
  useEffect(()=>{
    dispatch(getAsyncTodos());
  }, []);
  return (
    <div>
        <h2>TodoList</h2>
        {loading ? (
          <p>Loading...</p>
          ) : (
              <ul className="list-group">
              ok we are running here 
               {todos.map((todo)=>(
                  <TodoItem key={todo.id} {...todo} />
               ))}
              </ul>
            )
           }
    </div>
  )
}

export default TodoList