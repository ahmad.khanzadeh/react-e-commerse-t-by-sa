import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)

/* 
import ReactDOM from "react-dom/client";

function App(){
  return <h1> Hi react Gangs </h1>
}

const root=document.getElementById("Root");

ReactDOM.createRoot(root).render(<App />);

*/
