import CourseCard from "./CourseCard";
const courses=[
    {
        id:1,
        title:"English Lecture",
        description:"language lessons with the most popular teachers",
        imageUrl:"/1.jpg",
        rate:"4.5",
        tags:["languages"],
        start:"2023-07-01T20:46:30.615Z",
        status:"Completed",
    },
    {
        id:2,
        title:"Design Strategy",
        description: "lesson on planning design concept and proper Planning of ",
        imageUrl:'/2.jpg',
        rate:"4",
        tags:["UI/UX design","web design"],
        start:"2023-07-01T20:46:30.615Z",
        status:"Upcoming",
    
    },
    {
        id:3,
        title:"Business Lecture",
        description:'Lectures on how to build your business safely without fare',
        imageUrl:'/3.jpg',
        rate:"3.9",
        tags:["Marketing","Finance"],
        start:"2023-07-01T20:46:30.6115Z",
        status:"Active",
    },
];

function CourseList(){
    return(
        <div className="course-list">
        {courses.map((course)=>{
            return(
                <div key={course.id} className="course-item">
                    <CourseCard key={course.id}  course={course}/>
                </div>
            );
        })}
        
    </div>
    );
}

export default CourseList;