import React from 'react'

const Footer = () => {
  return (
    <footer className='footer'>
        You have successfully complete <em>33%</em> of your courses
    </footer>
  )
}

export default Footer