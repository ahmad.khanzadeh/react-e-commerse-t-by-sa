import React from 'react'

const allStatuses=["All", "Active", "Completed", "Upcomming"]
const Header = () => {
  return (
    <div>
        <h1>My Course (3)</h1>
        <div className='course-status'>
            {allStatuses.map(s => (<div key={s}>{s}</div>))}
        </div>
    </div>
    
  )
}

export default Header