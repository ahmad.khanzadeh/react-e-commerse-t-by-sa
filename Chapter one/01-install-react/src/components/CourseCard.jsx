import React from 'react'

const CourseCard = ({course}) => {
    
  return (
    <div className='course-item' key={course}>
                  <div className="course-item__img">
                       {/* <img src="/public/1.jpg" alt="course image 1"/> */}
                       <img src={course.imageUrl} alt={course.title}/>
                   </div>
              <div className="course-item__container">
                <CourseContainer  course={course}/>
                <CourseCardFooter course={course}/>
          </div>
    </div>
  )
}

export default CourseCard

function CourseContainer({course}){
  return(            <div className="course-item__detail">
  <div className="course-item__body">
  <div>
<p className="title">{course.title}</p>
<p className="desc">{course.description}</p>
</div>
<span className="rate">{course.rate}</span>
</div>
</div>);
}

function CourseCardFooter({course}){
  const startetAt=new Date(course.start).toLocaleDateString("en-US",{month:"short",year:"numeric",day:"numeric",});
  return(<div className="course-footer__container">
  <div className="tags">
  {course.tags.map(t =>{return(<span key={t} className="badge badge--secondary">{t}</span>)})}
     {/* <span className="badge badge--secondary">React.js</span>
     <span className="badge badge--secondary">Frontend</span> */}
  </div>
  <div className="caption">
     {/* <div className="date">{new Date(course.start).toLocaleDateString("en-US",{month:"short",year:"numeric",day:"numeric",})}</div> */}
     <div className="date">{startetAt}</div>
     <div className={`badge ${course.status==="Active" ? "badge--primary": course.status==="Upcoming"? "badge--danger" : "badge--secondary"}`}>{course.status}</div>
  </div>
  
</div>);
}

