import { useState } from 'react';
import './App.css'
const tabData=[
  {
    id:1,
    title: "ITEM 1",
    content: "Authourize the user data",
  },
  {
    id:2,
    title: "ITEM 2",
    content:"Redirect user to cart page",
  },
  {
    id:3,
    title: "ITEM 3",
    content:"Create new payment for the user",
  },
]
function App() {
  const [activeTab,setActiveTab]=useState(1);
  const [displayTab, showDisplayTab] = useState(false);
  const[count, setCount]=useState(0);
  const handleActiveTab = (id) =>{
    console.log(`the number ${id} was clicked`);
    setActiveTab(id);
    setCount((count) => count+1);
    console.log(count);
  }
  const showContent =()=>{
    showDisplayTab(!displayTab);
  }
  return (
    <div>
    <button onClick={showContent}>&times;</button>
    {displayTab ?     <div className="tab">
      <div className='tab--header'>
        <button className='active'>
          <span> Item one</span>
          <span className="tab-indicator"></span>
        </button>
        {
          tabData.map(tab =>(
            <button 
            key={tab.id} 
            className={activeTab == tab.id ? "activetab" : "" }
            onClick={() => handleActiveTab(tab.id)}
            >
                 <span>{tab.title}</span>
                 <span className="tab-indicator"></span>
             </button>
          ))
        }
      </div>
      <div className='tab__content'>{tabData[activeTab-1].content}</div>
    </div> : <div>Nothing to display- working area clean</div>}
    {/* <div className="tab">
      <div className='tab--header'>
        <button className='active'>
          <span> Item one</span>
          <span className="tab-indicator"></span>
        </button>
        {
          tabData.map(tab =>(
            <button 
            key={tab.id} 
            className={activeTab == tab.id ? "activetab" : "" }
            onClick={() => handleActiveTab(tab.id)}
            >
                 <span>{tab.title}</span>
                 <span className="tab-indicator"></span>
             </button>
          ))
        }
      </div>
      <div className='tab__content'>{tabData[activeTab-1].content}</div>
    </div> */}
    </div>
  )
}

export default App
