import './App.css'
import Navbar, { Favorites, SearchResult } from './assets/components/Navbar'
import CharacterDetail from './assets/components/CharacterDetail'
import CharacterList from './assets/components/CharacterList'
import { allCharacters } from '../data/data'
import { useEffect, useState } from 'react'
import Loader from './assets/components/Loader'
import toast, { Toaster } from 'react-hot-toast'
import axios from 'axios'
import Modal from './assets/components/Modal'
import { Search } from './assets/components/Navbar'
import useCharacter from './assets/hooks/useCharacter'
import useLocalStorage from './assets/hooks/useLocalStorage'


function App() {
  // const [characters,setCharacters]= useState([]);
  // const [isLoading, setIsLoading] = useState(false);
  // query is used for searched keyword by user-send to navbar
  const [query, setQuery]=useState();
  // fetch data form server using cusom hoock    
  const {isLoading, characters}= useCharacter("https://rickandmortyapi.com/api/character/?name",query)
  // which element is selected by user
  const [selectedId, setSelectedId]=useState(null);
  //save selected data into localStorage using custom hook
  const [favorites, setFavorites] = useLocalStorage("FAVOURITES",[]);


// the function which check which element was clicked
const handleSelectCharacter=(id)=>{
  //if older element was selected, igonr it
    setSelectedId(prevId => prevId==id ? null : id);
};

const handleAddFavorite=(char)=>{
  setFavorites((preFav) =>[...preFav, char]);
}
//romove from favorite list- in modal component-navbar component
const handleDeleteFavorite=(id)=>{
    setFavorites((prefav) =>prefav.filter((fav) => fav.id !== id));
}
const isAddedToFavourite = favorites.map((fav)=>fav.id).includes(selectedId);
  return (
      <div className='main-container app'>
        <Toaster />
        {/* <Modal modal="modal test from here"  open={true}> this is Children</Modal> */}
        <Navbar query={query} setQuery={setQuery} numOfResult={characters.length}> 
            <SearchResult numOfResult={characters.length} />
            <Favorites theFavourites={favorites} onDeleteFavourite={handleDeleteFavorite}/>
        </Navbar>
        <div className='main' >
        {isLoading==="true" ? <Loader /> : <CharacterList selectedId={selectedId} Characters={characters} isLoading={isLoading} onSelectCharacter={handleSelectCharacter}/>}
          {/* <CharacterList Characters={characters}/> */}
          <CharacterDetail isAddedToFavourite={isAddedToFavourite} onAddFavourite={handleAddFavorite} selectedId={selectedId}/>
        </div>
      </div>
  )
}

export default App
