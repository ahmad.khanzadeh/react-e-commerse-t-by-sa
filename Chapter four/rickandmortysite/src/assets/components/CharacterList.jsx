import { useState } from "react";


function CharacterList ({selectedId,Characters, children, isLoading,onSelectCharacter}) {
  if(isLoading){
    return(
      <div className='characters-list'>It is loading...</div>
    )
  }
  
  return (
    <div className="characters-list">
      {Characters.map((item)=>(
        <Character  key={item.id} item={item} >
        <button className="icon eye-for-elements" onClick={() => onSelectCharacter(item.id)}>
              {selectedId == item.id ? `displayed` : ""}
              &#128065;
          </button>
        </Character>
      ))}
    </div>
  )
}

export default CharacterList;

// export for like component in navbar
export function Character ({key, item,children}){
 
  return(<div key={key} className="list__item">
          <img src={item.image} alt={item.title} />
          <div className="infocard-container">
          <CharacterName item={item} />
          <CharacterInfo item={item} />
          </div>
          {children}
      </div>
  );
}

function CharacterName({item}){
  return(<h3 className="name">
    <span>{item.gender=="Male" ? "👨" : "👧"}</span>
    <span>{item.name}</span>
</h3>
  );
}

function CharacterInfo({item}){
  return(<div className="list-item__info">
  <span className={`status ${item.status === "Dead" ? "red": "green"}`}>{item.status === "Dead" ? "⚰️": "✔"}</span>
  <span>{item.status}</span>
  <span>- {item.species}</span>
</div>
  );
}