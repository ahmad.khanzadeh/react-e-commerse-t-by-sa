import { HeartIcon, TrashIcon } from '@heroicons/react/24/solid'
import Modal from './Modal';
import { useState } from 'react';
import { Character } from './CharacterList';

export default function Navbar ({children, query, setQuery,numOfResult}){
  return (
    <nav className="navbar">
        <Logo />
        <Search query={query} setQuery={setQuery}/>
        {/* <SearchResult numOfResult={numOfResult} >{children}</SearchResult> */}
        {/* <Favorites /> */}
        {children}
    </nav>
  )
}

function Logo(){
  return(<div className="navbar__log">Logo here</div>);
}

export function Search({query, setQuery}){
  return(<input 
    value={query}
    onChange={(e) => setQuery(e.target.value)}
    type="text"
    className="text-field"
    placeholder="search..."
    />);
}

export function SearchResult({numOfResult}){
  return(<div className="navbar__result"> Found {numOfResult} characters</div>);
}

export function Favorites({theFavourites,onDeleteFavourite}){
  const [isOpen, setIsOpen]=useState(false);
  return(<>
  <Modal onOpen={setIsOpen} open={isOpen} title="let it go"> 
    {theFavourites.map(item => {<Character 
                                  key={item.id}
                                  item={item} 
                                  >
                                      <button className='icon red' onClick={()=> onDeleteFavourite(item.id)}>
                                          <TrashIcon />
                                      </button> 
                                  </Character>
                                  }
                      )}
  </Modal>
    <button className='heart' onClick={()=> setIsOpen((is) =>!is)}>
            <HeartIcon className='icon' />
            <span className="badge">{theFavourites.length}</span>
        </button>
        </>
  );
  
}
