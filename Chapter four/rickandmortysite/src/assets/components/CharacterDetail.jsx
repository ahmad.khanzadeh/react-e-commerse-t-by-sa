import axios from "axios";
// import { episodes } from "../../../data/data"
import {useState, useEffect} from "react"
import Loader from "./Loader";
import { toast } from "react-hot-toast";


function CharacterDetail ({selectedId, onAddFavourite,isAddedToFavourite}) {
  const [character, setCharacter]=useState(null);
  const [isLoading, setIsLoading]=useState(false);
  const [episodes, setEpisodes]=useState([]); 

  useEffect(()=>{
    async function fetchSingleCharacterData() {
     try{
      setIsLoading(true);
      setCharacter(null);
      const { data }=await axios.get(`https://rickandmortyapi.com/api/character/${selectedId}`);
      setCharacter(data);

      // extract the number of episodes that chat plays: make the obj to an array and display the last one
     const episodesId= data.episode.map((e) => e.split("/").at(-1));
     
     //get thoese elements
     const { data: episodeData}=await axios.get(`https://rickandmortyapi.com/api/episode/${episodesId}`);
     setEpisodes([episodeData].flat().slice(0,3));
     
     }catch (error){
      toast.error(error.response.data.error)
     }finally{
      setIsLoading(false);
     }
    }

    // if nothing was searched, don't send fetch request
    if(selectedId) fetchSingleCharacterData();
  },[selectedId]);


  if(!character || !selectedId){
    return(
      <div style={{flex:1, color:"var(--slate-300"}}>
        Please select a character.
      </div>
    );
  }
  
  if(isLoading){
    return(<div style={{flex:1}}>
        <Loader />
    </div>

    )
  }
  return (
    <div style={{ flex:1 }}>
      <CharacterSubInfo character={character} isAddedToFavourite={isAddedToFavourite} onAddFavourite={onAddFavourite}/>
      {/* the small text under the card  */}
      <EpisodeList episodes={episodes} />
      
    </div>
  )
}

export default CharacterDetail

function CharacterSubInfo({character,isAddedToFavourite, onAddFavourite}){
  return(<div className="character-detail">
        <img 
        src={character.image} 
        alt={character.name} 
        className="character-detail__img"
        />
        <div className="character-detail__info">
          <h3 className="name">
          <span>{character.gender ==="Male" ? "👨🏻" : "👩🏻"}</span>
            <span>&nbsp;{character.name}</span>
          </h3>
          <div className="info">
              <span className={`status ${character.status ==="Dead" ? "red" : ""}`}></span>
              <span>&nbsp;{character.status}</span>
              <span> - &nbsp;{character.species}</span>
          </div>
          <div className="location">
              <p>Last known location :</p>
              <p> &nbsp; {character.location.name}</p>
          </div>
          <div className="actions">
          {isAddedToFavourite ? (<p>Already added to favorite</p>) : (
            <button onClick={() => onAddFavourite(character)} className="btn btn--primary">Add to Favorite</button>
          )}
              
          </div>
        </div>
      </div>
  );
}

function EpisodeList({episodes}){
  //if user click the button, toggle dating by earliest to latest
  // true => earliest => asc
  const [sortBy, setSortby]=useState(true);
  let sortedEpisodes = episodes;

  if(sortBy){
    sortedEpisodes=[...sortedEpisodes].sort((a,b)=> new Date(a.created) - new Date(b.created));
  } else{
    sortedEpisodes=[...sortedEpisodes].sort((a,b) => new Date(b.created) - new Date(a.created));
  }
   
  return(<div className="character-episodes">
    <div className="title">
      <h2> List of episode</h2>
      <button onClick={()=> setSortby((is) => !is)} style={{rotate:sortBy ? "0deg" : "180deg", transition:'all 0.3s ease-in'}}>
           &uarr;
      </button>
    </div>
    <ul>
      {
        sortedEpisodes.map((item,index) => {
          return(
            <li key={item.id}>
              <div>
                {String(index+1).padStart(2,"0")} &nbsp; -  {item.episode} : <strong>{item.name}</strong>
              </div>
              <div className="badge badge--secondary">-  &nbsp; - {item.air_date}</div>
            </li>
          )
        })
      }
    </ul>
  </div>
  );
}