import { useEffect, useState } from "react";

export default function useLocalStorage(key,initialState){
    //favorite list
  const [value, setValue]=useState(
    () => JSON.parse(localStorage.getItem(key)) || initialState
    );
    //useEfect: saving data into the local storage
    useEffect(()=>{
        localStorage.setItem(key,JSON.stringify(value))
      },[value])
      
  return [value, setValue];
}