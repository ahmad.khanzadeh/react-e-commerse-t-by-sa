import axios from "axios";
import { useState, useEffect } from "react";
import toast, { Toaster } from 'react-hot-toast'

export default function useCharacter(url,query){
  //  useEffect : fetching data from server, displaying error, cleanup function
    const [characters,setCharacters]= useState([]);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(()=>{
        //controller for clean up function
        const controller=new AbortController();
        const signal= controller.signal;
    //  useEffect : fetching data from server, displaying error, cleanup function
        async function fetchRickData(){
          try{
            setIsLoading(true);
            const {data}= await axios.get(`${url}=${query}`,{signal:signal})
            setCharacters(data.results.slice(0,6));
          }catch(error){
            if(!axios.isCancel()){
              // if there were some errors, empty the sidebar too.
              setCharacters([]);
              toast.error(error.response.data.error);
            }
    
          } finally{
            setIsLoading(false);
          }
    
        }
        fetchRickData();
        // clean up function
        return() =>{
          controller.abort();
    
        };
      },[query]);

      return {isLoading, characters};
}