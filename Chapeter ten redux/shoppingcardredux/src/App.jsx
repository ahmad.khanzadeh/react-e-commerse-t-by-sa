import './App.css'
import CakeContainer from './components/CakeContainer'
import MilkContainer from './components/MilkContainer'
import UsersList from './components/UsersList'
import store from './redux/store'
import { Provider } from 'react-redux'

function App() {
  
  return (
    <Provider store={store}>
      <div>Redux demo</div>
      <CakeContainer />
      <MilkContainer />
      <hr />
      <UsersList />
    </Provider>
  )
}

export default App
