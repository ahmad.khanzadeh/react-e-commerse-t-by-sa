import React from 'react'
import { useSelector, useDispatch } from "react-redux"
import { buyMilk } from '../redux/milk/milkActions';


const MilkContainer = () => {
   const milk= useSelector((state) => state.milk);
   const dispatch = useDispatch();
  return (
    <div>
        <h2>Number of milks in the fridge: {milk.numOfMilks}</h2>
        <button onClick={() => dispatch(buyMilk())}> Click for Milk</button>
    </div>
  )
}

export default MilkContainer