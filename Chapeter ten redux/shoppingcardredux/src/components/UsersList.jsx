import React from 'react'
import { useEffect } from 'react'
import { fetchUsers } from '../redux/user/userActions'
import {useDispatch , useSelector} from 'react-redux';

const userList = () => {
    const dispatch= useDispatch();

    useEffect(()=>{
        dispatch(fetchUsers());
    }, [dispatch]);

    // extract states from <rootReducer.js />
    const state=useSelector((state) => state.users);

    console.log(state);
  return (
    <div>userList
    {
      state.loading ? 
      (<p>Loading</p>) 
      : state.error ?
       (<p>{state.error}</p>)
       :
       (<div>
        {state.data && state.data.map(user => <li key={user.id}>{user.name}</li> )}
       </div>)
    }
    </div>
  )
}

export default userList