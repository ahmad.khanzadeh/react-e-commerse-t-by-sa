import { FETCH_USERS_FAILURE, FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS } from "./userTypes";
import axios from "axios";

function fetchUsersRequest(){
    return(
        {type: FETCH_USERS_REQUEST,}
    )
}

export const fetchUsers=() =>{
    return (dispatch) => {
        // dispatch({type: FETCH_USERS_REQUEST});
        dispatch(fetchUsersRequest());
        axios.get("https://jsonplaceholder.typicode.com/users")
        .then((res) => dispatch({type: FETCH_USERS_SUCCESS, payload: res.data}))
        .catch((err) => dispatch({type:FETCH_USERS_FAILURE, payload: err.message}));
    };
};