import { BUY_CAKE } from "./cakeTypes";

// create an action creator
export function buyCake(payload=1){
    return{
        type: BUY_CAKE, payload: payload
    };
}