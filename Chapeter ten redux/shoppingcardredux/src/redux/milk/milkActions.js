import { BUY_MILK } from "./milkTypes";

// create an action creator
export function buyMilk(payload=1){
    return{
        type: BUY_MILK, payload: payload,
    };
}