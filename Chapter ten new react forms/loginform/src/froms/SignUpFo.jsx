import React from 'react'
import { useState } from 'react';



const SignUpFo = () => {
const [userData,setUserData]=useState({ name:"", email:"", password:""})

const changehandler=(e)=>{
    setUserData({...userData,[e.target.name]:e.target.value})
    console.log(userData);
}

const submitHandler=(e)=>{
  e.preventDefault();
  console.log("submitted....")
}
  return (
    <div>
    <form onSubmit={submitHandler}>
      <div className="formControl">
        <label>Name</label>
        <input type="text" onChange={changehandler} name="name" value={userData.name}/>
      </div>
      <div className="formControl">
        <label>Email</label>
        <input type="text" onChange={changehandler} name="email" value={userData.email}/>
      </div>
      <div className="formControl">
        <label>Password</label>
        <input type="text" onChange={changehandler} name="password" value={userData.password}/>
      </div>
      <button>submit</button>
    </form>
  </div>
  )
}

export default SignUpFo