import { useTheme } from "../context/ThemeContext";


export default function Panel({title, children}){
    //with the same name that was exported in parent-remember to remove it from props
    const {theme, setTheme}=useTheme();
    const className="panel-"+theme;
    return(
        <section className={className}>
            <h1>{title}</h1>
            {children}
        </section>
    )
}