import { createContext, useContext, useState } from "react";


 const ThemeContext = createContext();

export function ThemeProvider({children}){
    const [theme, setTheme] = useState("light");
    return(
        <ThemeContext.Provider value={{theme, setTheme}}>{children}</ThemeContext.Provider>
    );
};

//make a custom hook to avoid importing useContext and ThemeContext

// export function useTheme(){
//     return useContext(ThemeContext);
// }

export const useTheme=() => {
    const context= useContext(ThemeContext);
    if (context===undefined) throw new Error("Themecontext was used outside of ThemeProvider");
    
    return context;
}